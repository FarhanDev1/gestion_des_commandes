<?php

namespace App\Http\Controllers;

use App\Models\Commande;
use App\Models\Client;
use App\Models\Produit;
use App\Models\DetailCommande;
use Illuminate\Http\Request;

class CommandeController extends Controller
{

    public function index()
    {
        // Afficher les commandes
        // $commandes = Commande::all();
        $commandes = Commande::paginate(10);
        return view('commandes.index', compact('commandes'));
    }

    public function create()
    {
        // Afficher le formulaire pour Ajouter une commande 
        $clients = Client::all();
        $produits = Produit::all();
        return view('commandes.create',compact('clients','produits'));
    }

    public function store(Request $request)
    {
        // Valider les données du formulaire
        $request->validate([
            'client_id' => 'required',
            'date_commande' => 'required',
            'produit_id' => 'required',
            'quantite' => 'required',
        ]);
        // Enregistrer la nouvelle commande
        $commande = new Commande();
        $commande->client_id = $request->client_id;
        $commande->date_commande = $request->date_commande;
        $commande->save();
        // Enregistrer les détails de commande
        $commande->detailsCommandes()->create([
            'commande_id'=>$commande->id,
            'produit_id' => $request->produit_id,
            'quantite' => $request->quantite,

        ]);
        // Rediriger vers la liste des commandes avec un message de succès
        return redirect()->route('commandes.index')->with('success', 'La commande a été ajoutée avec succès.');
    }

    public function show(Commande $commande)
    {
        // Afficher les détails de commande
        return view('commandes.show',compact('commande'));

    }

    public function edit(Commande $commande)
    {
        // Afficher le formulaire pour modifier une commande existante
        $produits = Produit::all();
        return view('commandes.edit', compact('commande','produits'));
    }


    public function update(Request $request,$id)
    {
        // Valider les données du formulaire
        $request->validate([
            'client_id' => 'required',
            'date_commande' => 'required',
            'produit_id' => 'required',
            'quantite' => 'required',
        ]);

        // Enregistrer la nouvelle commande
        $commande=Commande::findorFail($id);
        $commande->update([
            'client_id'=> $request->client_id,
            'date_commande' => $request->date_commande,
        ]);

        // Enregistrer les détails de commande
        $commande->detailsCommandes()->update([
            'commande_id'=>$id,
            'produit_id' => $request->produit_id,
            'quantite' => $request->quantite,
        ]);

        // Rediriger vers la liste des commandes avec un message de succès
        return redirect()->route('commandes.index')->with('success', 'La commande a été modifiée avec succès.');
    }

    public function destroy(Commande $commande)
    {
        // Supprimer la commande et les détails de commande associés
        $commande->detailsCommandes()->delete();
        $commande->delete();
        return redirect()->route('commandes.index')->with('success', 'La commande a été Supprimiée avec succès.');
    }
}
