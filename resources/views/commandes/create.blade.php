@extends('layouts.master')

@section('main')
    <div class="container">
        <h1>Ajouter une nouvelle commande</h1>

        <form action="{{ route('commandes.store') }}" method="post">
            @csrf

            <div class="form-group my-2">
                <label for="date">Date de commande</label>
                <input type="date" name="date_commande" id="date" class="form-control">
            </div>

            <div class="form-group my-2">
                <label for="client_id">Client</label>
                <select name="client_id" id="client_id" class="form-control">
                    @foreach ($clients as $client)
                        <option value="{{ $client->id }}">{{ $client->nom }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group my-2">
                <label for="produit_id">Produit</label>
                <select name="produit_id" id="produit_id" class="form-control">
                    @foreach ($produits as $produit)
                        <option value="{{ $produit->id }}">{{ $produit->nom }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group my-2 ">
                <label for="quantite">Quantite</label>
                <input type="number" name="quantite" id="quantite" class="form-control">
            </div>



            <button type="submit" class="btn btn-primary">Ajouter la commande</button>
            <a href="{{ route('commandes.index') }}" class="btn btn-secondary">Annuler</a>
        </form>
    </div>
@endsection