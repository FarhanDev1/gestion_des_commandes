@extends('layouts.master')

@section('main')
    <div class="container">
        <h1>Modifier une commande</h1>

        <form action="{{ route('commandes.update',$commande->id) }}" method="post">
            @csrf
            @method('PUT')
            <div class="form-group my-2">
                <label for="date">Date de commande</label>
                <input type="date" name="date_commande" value="{{$commande->date_commande}}" id="date" class="form-control">
            </div>

            <input type="hidden" name="client_id" value="{{$commande->client->id }}">

            <div class="form-group my-2">
                <label for="produit_id">Produit</label>
                <select name="produit_id" id="produit_id" class="form-control">
                    @foreach ($produits as $produit)
                        <option value="{{ $produit->id }}">{{ $produit->nom }} prix :{{ $produit->prix }}DH </option>
                    @endforeach
                </select>
            </div>

            <div class="form-group my-2 ">
                <label for="quantite">Quantite</label>
                @foreach($commande->detailsCommandes as $detail)
                    <input type="number" name="quantite" value="{{$detail->quantite}}" id="quantite" class="form-control">
                @endforeach
            </div>



            <button type="submit" class="btn btn-primary">Modifier la commande</button>
            <a href="{{ route('commandes.index') }}" class="btn btn-secondary">Annuler</a>
        </form>
    </div>
@endsection