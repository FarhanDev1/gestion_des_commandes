@extends('layouts.master')
@section('main')
    
<div class="container">
    <h1>Liste des commandes</h1>
    @if (session()->has('success'))
        <div class="alert alert-success">
            <strong>{{session('success')}}</strong>
        </div>
    @endif
    <table class="table">
        <thead>
            <tr>
                <th>Numéro</th>
                <th>Date</th>
                <th>Client</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($commandes as $commande)
                <tr>
                    <td>{{ $commande->id }}</td>
                    <td>{{ $commande->date_commande }}</td>
                    <td>{{ $commande->client->nom }}</td>
                    <td>
                        <a href="{{ route('commandes.edit', $commande) }}" class="btn btn-primary">Modifier</a>
                        <a href="{{ route('commandes.show', $commande) }}" class="btn btn-success">detailsCommandes</a>
                        <form action="{{ route('commandes.destroy', $commande) }}" method="post" class="d-inline">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger" onclick="return confirm('Êtes-vous sûr de vouloir supprimer cette commande ?')">Supprimer</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{ $commandes->links('pagination::bootstrap-5') }}
</div>


@endsection