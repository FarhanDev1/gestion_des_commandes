@extends('layouts.master')

@section('main')
    <div class="container">
        <h1>Détails de la commande #{{ $commande->id }}</h1>
        <div class="row">
            <div class="col-md-6">
                <p>Date : {{ $commande->date_commande }}</p>
                <p>Client : {{ $commande->client->nom }}</p>
            </div>
            <div class="col-md-6">
                <div class="d-grid gap-2 d-md-block">
                    <a class="btn btn-primary" href="#">Ajouter des produits à la commande</a>
                </div>
            </div>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th>Produit</th>
                    <th>Quantité</th>
                    <th>Prix unitaire</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                @foreach($commande->detailsCommandes as $detail)
                    <tr>
                        <td>{{ $detail->produit->nom }}</td>
                        <td>{{ $detail->quantite }}</td>
                        <td>{{ $detail->produit->prix}} DH</td>
                        <td>{{ $detail->quantite * $detail->produit->prix }} DH</td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <p>Total de la commande : {{ $commande->total }}</p>

        <a href="{{ route('commandes.index') }}" class="btn btn-secondary">Retour à la liste des commandes</a>
    </div>
@endsection