@extends('layouts.master')

@section('main')
<div class="container">
    <h1>Ajouter un produit</h1>

    <form action="{{ route('produits.store') }}" method="post">
        @csrf

        <div class="form-group my-2">
            <label for="nom">Nom de produit </label>
            <input type="text" name="nom" id="nom" class="form-control">
        </div>

        <div class="form-group my-2">
            <label for="description">Description </label>
            <input type="text" name="description" id="nom" class="form-control">
        </div>

        <div class="form-group my-2">
            <label for="prix">Prix</label>
            <input type="number" name="prix" id="prix" class="form-control">
        </div>

        <button type="submit" class="btn btn-primary">Ajouter le produit</button>
        <a href="{{ route('commandes.index') }}" class="btn btn-secondary">Annuler</a>
    </form>
</div>
@endsection